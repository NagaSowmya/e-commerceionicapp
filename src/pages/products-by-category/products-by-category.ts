import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams ,ToastController} from 'ionic-angular';
import * as WC from "woocommerce-api";
import { ProductDetailsPage } from "../product-details/product-details";


/**
 * Generated class for the ProductsByCategoryPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-products-by-category',
  templateUrl: 'products-by-category.html',
})
export class ProductsByCategoryPage {

  WooCommerce:any;
  products:any[];
  page:number;
  category:any;

  constructor(public navCtrl: NavController, public navParams: NavParams,public toastCtrl: ToastController) {
    this.page=0;
    this.category=this.navParams.get("category");

    this.WooCommerce = WC({
      url: "http://localhost/wordpress",
      consumerKey: "ck_a0aacf101d6644d4996aab9cd19a5c661eb81187",
      consumerSecret: "cs_6e7040e7246b23648c16d5e2403d641210bd17ee"
    });

    this.WooCommerce.getAsync("products?filter[category]="+this.category.slug).then((data)=>{
      console.log(JSON.parse(data.body));
      this.products=JSON.parse(data.body).products;
    },(err)=>{
      console.log(err);
    })

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ProductsByCategoryPage');
  }

  loadMoreProducts(event){
    this.page++;
    console.log("Getting the page:"+this.page);
    this.WooCommerce.getAsync("products?filter[category]="+this.category.slug+"&page="+this.page).then((data)=>{
      let temp=(JSON.parse(data.body).products);
      this.products=this.products.concat(JSON.parse(data.body).products);
      console.log(this.products);
      event.complete();
      if(temp.length<8)
      event.enable(false);
    });
  }

  
  // loadMoreProducts(event){
  //   if(event==null){
  //     this.page=2;
  //     // this.products=[];
  //     // this.moreProducts=[];
  //   }
  //   else
  //   this.page ++;
  //   console.log("Getting the Page"+this.page);
  //   this.WooCommerce.getAsync("products?filter[category]="+this.category.slug).then((data)=>{
  //     console.log(JSON.parse(data.body));
  //     // this.products=this.products.concat(JSON.parse(data.body).products);
  //     if(event !=null){
  //       event.complete();
  //     }
  //     if(JSON.parse(data.body).products.length <6){
  //       event.enable(false);

  //       this.toastCtrl.create({
  //         message:"No more products!",
  //         duration:5000
  //       }).present();
  //     }
  //   },(err)=>{
  //     console.log(err);
  //   })
  // }

  onProductPage(product){
    this.navCtrl.push(ProductDetailsPage,{"product":product});
  }

}

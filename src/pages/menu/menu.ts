import { Component,ViewChild } from '@angular/core';
import {  NavController, NavParams ,ModalController} from 'ionic-angular';
import {  HomePage  } from "../home/home";
import * as WC from "woocommerce-api";
import { ProductsByCategoryPage } from "../products-by-category/products-by-category";
import { SignupPage } from "../signup/signup";
import { LoginPage } from "../login/login";
import { Storage } from '@ionic/storage';
import { CartPage } from '../cart/cart';


@Component({
  selector: 'page-menu',
  templateUrl: 'menu.html',
})
export class MenuPage {

  WooCommerce: any;
  categories:any[];
  homePage: any;
  loggedIn:boolean;
  user:any;

@ViewChild('content') childNavCtrl:NavController;
  constructor(public navCtrl: NavController, public navParams: NavParams,
    public storage:Storage,public modalCtrl: ModalController) {
    this.homePage = HomePage;
    this.categories=[];
    this.user={};
    this.WooCommerce = WC({
      url: "http://localhost/wordpress",
      consumerKey: "ck_a0aacf101d6644d4996aab9cd19a5c661eb81187",
      consumerSecret: "cs_6e7040e7246b23648c16d5e2403d641210bd17ee"
    });

    this.WooCommerce.getAsync('products/categories').then((data)=>{
      console.log(JSON.parse(data.body).product_categories);
      let temp:any[]=JSON.parse(data.body).product_categories;
      for(let i=0;i<temp.length;i++){
        if(temp[i].parent ==0){

          if(temp[i].slug== "uncategorized"){
            temp[i].icon="images";
          }
          if(temp[i].slug== "clothing"){
            temp[i].icon="shirt";
          }
          if(temp[i].slug== "decor"){
            temp[i].icon="cog";
          }
          if(temp[i].slug== "music"){
            temp[i].icon="musical-notes";
          }
          this.categories.push(temp[i]);

        }
      }

    },(err)=>{
      console.log(err);
    });
  }

  ionViewDidEnter() {
    this.storage.ready().then(()=>{
      this.storage.get("userLoginInfo").then((userLoginInfo)=>{
        if(userLoginInfo!=null){
          console.log("User Logged In..");
          this.user=userLoginInfo.user;
          console.log(this.user);
          this.loggedIn=true;
        }else{
          console.log("No user found.");
          this.user={};
          this.loggedIn=false;
        }
      })
    })
  }

  openCategoryPage(category){
    this.childNavCtrl.setRoot(ProductsByCategoryPage,{"category":category});
  }
  openPage(pageName:string){
    if(pageName=="signup"){
      this.navCtrl.push(SignupPage)
    }
    if(pageName=="login"){
      this.navCtrl.push(LoginPage);
    }
    if(pageName=="logout"){
      this.storage.remove("userLoginInfo").then(()=>{
        this.user={};
        this.loggedIn=false;
      });
      }
    if(pageName=="cart"){
      let modal=this.modalCtrl.create(CartPage);
      modal.present();
    }
  }
  
}
